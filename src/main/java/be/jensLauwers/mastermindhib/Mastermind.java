package be.jensLauwers.mastermindhib;



import be.jensLauwers.mastermindhib.database.MMLevels;
import be.jensLauwers.mastermindhib.tools.StringTool;

import java.util.Random;

public class Mastermind {

    public static final String FULL_VALID_GUESS = "+";
    public static final String SEMI_VALID_GUESS = "-";
    static final String THE_GAME_FINISHED = "The game was already finished!";

    private String invalidGuessStructure;
    private int guesses;
    private String answer;
    private boolean finishedGame;
    private MMLevels gameLevel;

    public MMLevels getGameLevel() {
        return gameLevel;
    }

    public void setGameLevel(MMLevels gameLevel) {
        if(gameLevel == null) throw new IllegalArgumentException("Empty level");
        this.gameLevel = gameLevel;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        if(answer == null) throw new IllegalArgumentException("Empty answer");

        String inputStructureRegex =
                "^[" + getGameLevel().getLowDigit() + "-" + getGameLevel().getHighDigit() + "]{" + getGameLevel().getNumOfDigits() + "}$";

        if (!answer.matches(inputStructureRegex))
            throw new IllegalArgumentException("The answer must contain " + gameLevel.getNumOfDigits() +
                    " digits, from " + gameLevel.getLowDigit() + " to " + gameLevel.getHighDigit() + "!");

        this.answer = answer;
    }

    public boolean isFinishedGame() {
        return finishedGame;
    }


    public void setFinishedGame(boolean finishedGame) {
        this.finishedGame = finishedGame;
    }


    public int getGuesses() {
        return guesses;
    }


    public void setGuesses(int guesses) {
        if(guesses < 0) throw new IllegalArgumentException("Negative guesses");
        this.guesses = guesses;
    }

    public String getInvalidGuessStructure() {
        return invalidGuessStructure;
    }


    public void setInvalidGuessStructure(String invalidGuessStructure) {
        if(invalidGuessStructure == null) throw new IllegalArgumentException("Empty structure");

        this.invalidGuessStructure = invalidGuessStructure;
    }

    public void reset(MMLevels gameLevel) {
        if(gameLevel == null) throw new IllegalArgumentException("Empty level");

        finishedGame = false;
        guesses = 0;
        createRandomAnswer(gameLevel);
    }

    public String checkGuess(String guess, MMLevels gameLevel) {

        // validation
        if(guess == null) throw new IllegalArgumentException("Empty guess");
        guess = guess.replaceAll("( )*", "");

        int lowNum = gameLevel.getLowDigit();
        int highNum = gameLevel.getHighDigit();
        int numDigits = gameLevel.getNumOfDigits();

        String inputStructureRegex = "^[" + lowNum + "-" + highNum + "]{" + numDigits + "}$";

        if (guess.length() < getAnswer().length()
                || guess.length() > getAnswer().length()
                || guess.length() == 0
                || !guess.matches(inputStructureRegex)) {
            throw new IllegalArgumentException(
                    "Guess must contain " + numDigits + " digits between " + lowNum +" and " + highNum);
        }

        setInvalidGuessStructure("Invalid: The guess needs to be " + numDigits +
                " digits, from " + lowNum + " to " + highNum + "!");

        if (isFinishedGame()) return THE_GAME_FINISHED;

        if (guess.isBlank()) return getInvalidGuessStructure();
        if (!guess.matches(inputStructureRegex)) return getInvalidGuessStructure();

        // actually check the guess
        setGuesses(getGuesses() + 1);

        String answer = getAnswer();
        String currentGuess = guess;
        StringBuilder correctPos = new StringBuilder();
        StringBuilder notCorrectPos = new StringBuilder();

        for(int i = 0; i < answer.length(); i++) {
            // Check match on the same position
            if(answer.charAt(i) == guess.charAt(i)) {
                correctPos.append(FULL_VALID_GUESS);
                answer = StringTool.setX(answer, i);

                if(gameLevel.isAllowDoubles()) {
                    currentGuess = StringTool.setX(currentGuess, i);
                }
            }
        }

        for (int i = 0; i < answer.length(); i++) {
            if(answer.charAt(i) == 'x') continue;

            // Check matches for all positions
            for (int j = 0; j < currentGuess.length(); j++) {
                if (i == j) continue;

                if (answer.charAt(i) == currentGuess.charAt(j) && currentGuess.charAt(j) != 'x') {
                    notCorrectPos.append(SEMI_VALID_GUESS);
                    answer = StringTool.setX(answer, i);

                    break; // Breaking out of guess loop since answer is all unique chars
                }
            }
        }

        String result = correctPos.toString() + notCorrectPos.toString();
        //# check if the game is finished
        if (result.matches("[" + FULL_VALID_GUESS + "]{" + numDigits + "}"))
            finishedGame = true;

        //# return result
        return result;
    }

    public void createRandomAnswer(MMLevels gameLevel) {
        if(gameLevel == null) throw new IllegalArgumentException("Empty level");

        int lowNum = gameLevel.getLowDigit();
        int highNum = gameLevel.getHighDigit();
        int numDigits = gameLevel.getNumOfDigits();

        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        int count = 0;

        do {
            String input;

            do {
                input = Integer.toString(rand.nextInt(highNum + 1));
            } while(!input.matches("[" + lowNum + "-" + highNum + "]"));

            if (!gameLevel.isAllowDoubles() && !sb.toString().contains(input)) {
                sb.append(input);
                count++;
            }

            if (gameLevel.isAllowDoubles()) {
                sb.append(input);
                count++;
            }

        } while (count < numDigits);

        setAnswer(sb.toString());
    }
}
