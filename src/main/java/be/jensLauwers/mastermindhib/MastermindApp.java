package be.jensLauwers.mastermindhib;

import be.jensLauwers.mastermindhib.database.*;
import be.jensLauwers.mastermindhib.tools.ConsoleInputTool;
import be.jensLauwers.mastermindhib.tools.ConsolePrintTool;

import java.sql.SQLException;
import java.util.List;

public class MastermindApp {
    Mastermind mastermind = new Mastermind();
    ConsoleInputTool consoleInput = new ConsoleInputTool();
    int gamesPlayed = 0;
    MMScores scores = new MMScores();
    MMScoresDAO scoresDAO = new MMScoresDAO();
    MMLevelsDAO levelsDAO = new MMLevelsDAO();
    String name;

    public static void main(String[] args) throws LevelException, ScoreException, SQLException {
        MastermindApp app = new MastermindApp();
        app.startGame();
    }

    public void startGame() throws LevelException, ScoreException, SQLException {
        this.name = consoleInput.askUserName("Please enter your name: ");
        startMenu();

        System.out.println("Bye, " + name + "!");
    }

    public int determineLevel() throws SQLException {
        int level;

        do {
            List<MMLevels> allLevels = levelsDAO.getAllLevels();

            allLevels.forEach(l -> {
                String output = ConsolePrintTool.ANSI_YELLOW + l.getId() + ". " + l.getLevelName() + ConsolePrintTool.ANSI_RESET;
                if (l.getId() != allLevels.size()) System.out.print(output + " | ");
                else System.out.print(output);
            });

            System.out.println("\r");

            level = consoleInput.askUserPosIntBetweenRange("Give a level between 1 and 3:", 1, 3);
            if (level < 1 || level > 3) System.err.println("Level cannot be smaller than 1 or higher than 3.");
        } while (level < 1 || level > 3);

        return level;
    }


    public void setLevelData(int levelId) throws LevelSQLException {
        if (levelId < 1 || levelId > 3) throw new IllegalArgumentException("levelId must be between 1 and 3.");

        mastermind.setGameLevel(levelsDAO.getLevelById(levelId));
    }

    void runGame() throws LevelException, ScoreException, SQLException {

        beforePlay();
        long startTime = System.currentTimeMillis();
        while (!mastermind.isFinishedGame()) {
            String guess;
            if (!mastermind.getGameLevel().isAllowDoubles()) {

                do {
                    System.out.println(mastermind.getAnswer());
                    guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(

                            "Make a guess: ",
                            mastermind.getGameLevel().getNumOfDigits(), mastermind.getGameLevel().getLowDigit(), mastermind.getGameLevel().getHighDigit());
                } while (!consoleInput.checkNoDoublesInString(guess));

            } else {
                System.out.println(mastermind.getAnswer());
                guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(
                        "Make a guess: ",
                        mastermind.getGameLevel().getNumOfDigits(), mastermind.getGameLevel().getLowDigit(), mastermind.getGameLevel().getHighDigit());
            }

            System.out.println("Answer: " + mastermind.checkGuess(guess, mastermind.getGameLevel()));
        }

        won(startTime);
    }


    private void beforePlay() throws SQLException {

        setLevelData(determineLevel());
        mastermind.reset(mastermind.getGameLevel());
        ConsolePrintTool.printTitle("Welcome to Mastermind " + mastermind.getGameLevel().getLevelName() + " level!");
        scores = new MMScores();
        scores.setMmLevel(mastermind.getGameLevel());
        scores.setPlayer(name);
        if (!scoresDAO.getScoresByPlayerName(name).isEmpty()) {
            gamesPlayed = scoresDAO.resumeGamesPlayed(name).getPlayedGames();
            scores.setPlayedGames(gamesPlayed);
        } else {
            scores.setPlayedGames(0);
        }

        System.out.println("I'm thinking of a " + mastermind.getGameLevel().getNumOfDigits() + " digit code. " +
                "Numbers starting from " + mastermind.getGameLevel().getLowDigit() + " up to " + mastermind.getGameLevel().getHighDigit() +
                ", duplicates" + (mastermind.getGameLevel().isAllowDoubles() ? " " : " not ") + "allowed.");

        System.out.println(Mastermind.FULL_VALID_GUESS + " represents a correct number guessed in the correct position.");
        System.out.println(Mastermind.SEMI_VALID_GUESS + " represents a correct number guessed in the wrong position.");
    }

    private void won(long startTime) {
        long endTime = System.currentTimeMillis();
        String duration = ConsolePrintTool.formatTime(endTime - startTime);
        System.out.println("Congratulations!");
        System.out.println("You guessed " + mastermind.getGuesses() + " times.");
        scores.setNumberOfGuesses(mastermind.getGuesses());
        scores.setGameTime(duration);
        gamesPlayed++;
        scores.setPlayedGames(gamesPlayed);
        ConsolePrintTool.printHeading();
        System.out.println(scores);
        scoresDAO.addMMScores(scores);
    }

    private void printMenu() {
        System.out.println("1. Play.");
        System.out.println("2. New Player");
        System.out.println("3. Overview of HighScores.");
        System.out.println("4. View previous scores by playerName.");
        System.out.println("5. Get top 10 for each level.");
        System.out.println("6. Exit the app.");
        ConsolePrintTool.printEnter();
    }

    public void startMenu() throws ScoreException, SQLException {
        int choice;
        do {
            System.out.println("Hello " + ConsolePrintTool.ANSI_YELLOW + name + ConsolePrintTool.ANSI_RESET + "!" + " Hope you are having a good day! Welcome to the starting menu," +
                    "please choose one of the following options: ");

            printMenu();

            choice = consoleInput.askUserPosIntBetweenRange("Input a number between 1 and 6", 1, 6);

            if (choice == 1) {
                do {
                    runGame();
                } while (consoleInput.askYesOrNo("Do you want to play again?(y/n): "));
                System.out.println(name + " played Mastermind " + gamesPlayed + " times.");
            }

            if (choice == 2) {
                this.name = consoleInput.askUserName("Please enter your name: ");
                if (scoresDAO.getScoresByPlayerName(name).isEmpty()) {
                    gamesPlayed = 0;
                    scores.setPlayedGames(gamesPlayed);
                } else if (!scoresDAO.getScoresByPlayerName(name).isEmpty()) {
                    gamesPlayed = scoresDAO.resumeGamesPlayed(name).getPlayedGames();
                    scores.setPlayedGames(gamesPlayed);
                }
            }
            if (choice == 3) {

                List<MMLevels> allLevels = levelsDAO.getAllLevels();
                allLevels.forEach(a -> {
                    MMScores highScore = scoresDAO.getHighscorePerLevel(a.getId());
                    ConsolePrintTool.printEnter();
                    ConsolePrintTool.printHeading();
                    if (highScore != null) System.out.println(highScore);
                });
                ConsolePrintTool.printEnter();
            }

            if (choice == 4) {
                String name = consoleInput.askUserName("Insert name: ");

                List<MMScores> scorePlayerList =scoresDAO.getScoresByPlayerName(name);
                ConsolePrintTool.printHeading();
                scorePlayerList.forEach(System.out::println);
                ConsolePrintTool.printEnter();
            }

            if (choice == 5) {

                List<MMLevels> allLevels = levelsDAO.getAllLevels();
                allLevels.forEach(l -> {
                    List<MMScores>scoresList =scoresDAO.getTenBestScoresPerLevel(l.getId());
                    ConsolePrintTool.printEnter();
                    ConsolePrintTool.printHeading();
                    scoresList.forEach(System.out::println);
                });
                ConsolePrintTool.printEnter();
            }
        }
        while (choice != 6);
    }
}

