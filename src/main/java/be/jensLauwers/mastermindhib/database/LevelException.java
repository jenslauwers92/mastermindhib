package be.jensLauwers.mastermindhib.database;

/**
 * Creating new class for handling the exceptions according to class
 */
public class LevelException extends IllegalArgumentException {
    public LevelException(String message) {
        super(message);
    }

}
