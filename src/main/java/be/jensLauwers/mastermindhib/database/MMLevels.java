package be.jensLauwers.mastermindhib.database;

import javax.persistence.*;

@Entity
@Table(name = "MMLevels")
public class MMLevels {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "LevelName")
    private String levelName;

    @Column(name = "LowDigit")
    private int lowDigit;

    @Column(name = "HighDigit")
    private int highDigit;

    @Column(name = "AllowDoubles")
    private boolean allowDoubles;

    @Column(name = "NumOfDigits")
    private int numOfDigits;



    public MMLevels() {

    }


    public MMLevels(String levelName, int lowDigit, int highDigit, int numOfDigits, boolean allowDoubles) {
        setLevelName(levelName);
        setHighDigit(highDigit);
        setLowDigit(lowDigit);
        setNumOfDigits(numOfDigits);
        setAllowDoubles(allowDoubles);
    }



    public int getId() {
        return id;
    }



    public void setId(Integer id) {
        if(id != null && id < 1) throw new LevelException("Negative Id");
        this.id = id;
    }


    public String getLevelName() {
        return levelName;
    }


    public void setLevelName(String levelName) {
        if(levelName == null) throw new LevelException("Empty level");
        this.levelName = levelName;
    }


    public int getLowDigit() {
        return lowDigit;
    }

    public void setLowDigit(int lowDigit) {
        if(lowDigit < 0) throw new LevelException("Negative lowDigit");
        if(lowDigit > highDigit) throw new LevelException("lowest bigger than highest digit");
        this.lowDigit = lowDigit;
    }

    public int getHighDigit() {
        return highDigit;
    }


    public void setHighDigit(int highDigit) {
        if(highDigit < lowDigit) throw new LevelException("highest digit lower than lowest digit");
        this.highDigit= highDigit;
    }


    public int getNumOfDigits() {
        return numOfDigits;
    }


    public void setNumOfDigits(int numOfDigits) {
        if(numOfDigits < 1) throw new LevelException("numOfDigits<1");
        this.numOfDigits = numOfDigits;
    }


    public boolean isAllowDoubles() {
        return allowDoubles;
    }


    public void setAllowDoubles(boolean allowDoubles) {
        this.allowDoubles = allowDoubles;
    }





}

