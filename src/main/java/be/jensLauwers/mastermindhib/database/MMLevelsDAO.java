package be.jensLauwers.mastermindhib.database;


import javax.persistence.*;
import java.util.List;

public class MMLevelsDAO implements MMLevelsDAO_Interface {
    private EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction et;

    public EntityManager getEm() {

        try {
            this.emf = Persistence.createEntityManagerFactory("course");
            this.em = emf.createEntityManager();
        } catch (PersistenceException pe) {
            pe.printStackTrace();
        }

        return this.em;

    }

    public EntityTransaction getEt() {
        try {
            this.et = this.em.getTransaction();
        } catch (PersistenceException pe) {
            pe.printStackTrace();
        }
        return this.et;


    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public void setEt(EntityTransaction et) {
        this.et = et;
    }

    @Override
    public List<MMLevels> getAllLevels() throws LevelException, LevelSQLException {
        List<MMLevels> lvlList;
        try {
            lvlList = getEm().createQuery("SELECT s FROM MMLevels s",MMLevels.class).getResultList();
            if(lvlList==null) throw new LevelException("No list found");
            return lvlList;
        } catch(Exception se) {
            throw new LevelSQLException(se);
        }
    }
    @Override
    public MMLevels getLevelById(int id) throws LevelSQLException {
        if(id < 0) throw new LevelException("Negative ID");
        MMLevels tempMMLevel;
        try {
            tempMMLevel = getEm().find(MMLevels.class,id);
            if(tempMMLevel==null)throw new LevelException("Empty level");
            return tempMMLevel;
        } catch (Exception e) {
            throw new LevelSQLException(e);
        }
    }

}

