package be.jensLauwers.mastermindhib.database;



import java.util.List;


public interface MMLevelsDAO_Interface {
    List<MMLevels> getAllLevels() throws LevelException, LevelSQLException;
    MMLevels getLevelById(int id) throws LevelException, LevelSQLException;
}

