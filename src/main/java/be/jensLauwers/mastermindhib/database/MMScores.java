package be.jensLauwers.mastermindhib.database;



import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "MMScores")
public class MMScores {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;



    @Column(name = "Player")
    private String player;
    @Column(name = "NumberOfGuesses")
    private int numberOfGuesses;
    @Column(name = "PlayedGames")
    private int playedGames;
    @Column(name = "GameTime")
    private String gameTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ScoreLevelId")
    private MMLevels mmLevel;

    public MMScores() {
    }

    public MMScores (String player, int numberOfGuesses, int playedGames, String gameTime) {

        setPlayer(player);
        setNumberOfGuesses(numberOfGuesses);
        setPlayedGames(playedGames);
        setGameTime(gameTime);
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        if (id != null && id < 0) throw new ScoreException("Wrong id");
        this.id = id;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        if (player == null || player.isBlank()) throw new ScoreException("Empty player");
        this.player = player;
    }

    public int getNumberOfGuesses() { return numberOfGuesses; }

    public void setNumberOfGuesses(int numberOfGuesses) {
        if(numberOfGuesses < 0) throw new ScoreException("can't be lower than 1");
        this.numberOfGuesses = numberOfGuesses;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        if(playedGames < 0) throw new ScoreException("Negative number");
        this.playedGames = playedGames;
    }

    public String getGameTime() {
        return gameTime;
    }

    public void setGameTime(String gameTime) {
        if(gameTime == null || gameTime.isBlank()) throw new ScoreException("timePlayed cannot be null or negative");

        this.gameTime = gameTime;
    }

    public MMLevels getMmLevel() {
        return mmLevel;
    }

    public void setMmLevel(MMLevels mmLevel) {
        this.mmLevel = mmLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MMScores mmScores = (MMScores) o;
        return numberOfGuesses == mmScores.numberOfGuesses &&
                playedGames == mmScores.playedGames &&
                Objects.equals(id, mmScores.id) &&
                Objects.equals(player, mmScores.player) &&
                Objects.equals(gameTime, mmScores.gameTime) &&
                Objects.equals(mmLevel, mmScores.mmLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, player, numberOfGuesses, playedGames, gameTime);
    }

    @Override
    public String toString() {
        return String.format("%10s |%9d|%8d|%10s|%6d",
                player, numberOfGuesses, playedGames, gameTime, mmLevel.getId());
    }
}


