package be.jensLauwers.mastermindhib.database;

import javax.persistence.*;
import java.util.List;


public class MMScoresDAO implements MMScoresDAO_Interface {
    private EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction et;

    public EntityManager getEm() {
        try {
            this.emf = Persistence.createEntityManagerFactory("course");
            this.em = emf.createEntityManager();
        } catch (PersistenceException pe) {
            pe.printStackTrace();
        }
        return this.em;
    }

    public EntityTransaction getEt() {
        try {
            this.et = this.em.getTransaction();
        } catch (PersistenceException pe) {
            pe.printStackTrace();
        }
        return this.et;
    }
    public MMScoresDAO() {
    }

    @Override
    public MMScores getScoreById(int id) {
        if (id < 0) throw new ScoreException("Negative ID");
        MMScores tempScore;
        try {
            this.em = getEm();
            this.et = getEt();
            et.begin();
            tempScore = em.find(MMScores.class, id);
            return tempScore;
        } catch (Exception e) {
            throw new PersistenceException("Something went wrong");
        }

    }
    @Override
    public List<MMScores> getScoresByPlayerName(String player) {
        if (player == null) throw new ScoreException("Empty player");
        List<MMScores> scores;

        try {
            scores = getEm().createQuery("SELECT p FROM MMScores p Where p.player = :player " +
                    "ORDER BY p.mmLevel.id ,p.numberOfGuesses", MMScores.class)
                    .setParameter("player", player).getResultList();
            return scores;
        } catch (Exception e) {
            throw new ScoreException(e);
        }

    }

    @Override
    public MMScores getHighscorePerLevel(int scoreLevelId) {
        if (scoreLevelId < 0) throw new ScoreException("Negative id");

        MMScores tempScore;
        try {

            tempScore = getEm().createQuery("SELECT s FROM MMScores s WHERE s.mmLevel.id = :levelid",MMScores.class)
                    .setParameter("levelid",scoreLevelId).setMaxResults(1).getSingleResult();

            return tempScore;
        } catch (Exception e) {
            throw new ScoreException(e);
        }

    }
    @Override
    public List<MMScores> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException {
        if (scoreLevelId < 0) throw new ScoreException("Negative id");
        List<MMScores> scoreList;
        try {
            scoreList = getEm().createQuery("SELECT l FROM MMScores l Where l.mmLevel.id = :levelid " +
                    "ORDER BY l.numberOfGuesses", MMScores.class)
                    .setParameter("levelid", scoreLevelId).setMaxResults(10).getResultList();
            return scoreList;
        } catch (Exception e) {
            throw new ScoreException(e);
        }
    }
    @Override
    public MMScores resumeGamesPlayed(String player) throws ScoreException {
        if (player == null) throw new ScoreException("Empty player");
        MMScores score;
        try {
            score = getEm().createQuery("SELECT s FROM MMScores s Where s.player = :player " +
                    "ORDER BY s.mmLevel.id ,s.numberOfGuesses ,s.gameTime", MMScores.class)
                    .setParameter("player", player).setMaxResults(1).getSingleResult();
            return score;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new ScoreException(e);
        }
    }

    @Override
    public void addMMScores(MMScores score) {
        if (score == null) throw new ScoreException("Empty score");

        try {
            em = getEm();
            em.getTransaction().begin();

            em.persist(score);
            em.getTransaction().commit();


        } catch (Exception e) {
            throw new ScoreException(e);
        }
        System.out.println("Score saved");
    }

    @Override
    public void updateMMScores(MMScores score) {
        MMScores temp;
        try {
            this.em = getEm();
            this.et = getEt();
            et.begin();
            temp = em.find(MMScores.class, score.getId());
            temp.setPlayer(score.getPlayer());
            temp.setNumberOfGuesses(score.getNumberOfGuesses());
            temp.setPlayedGames(score.getPlayedGames());
            temp.setGameTime(score.getGameTime());
            temp.setMmLevel(score.getMmLevel());
            et.commit();

        } catch (Exception e) {
            throw new ScoreException(e);
        }
        System.out.println("Updated");
    }
}

