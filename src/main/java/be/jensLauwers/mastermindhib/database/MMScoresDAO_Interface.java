package be.jensLauwers.mastermindhib.database;



import java.util.List;


public interface MMScoresDAO_Interface {

    MMScores getScoreById(int id) throws ScoreException;

    MMScores getHighscorePerLevel(int scoreLevelId) throws ScoreException;

    List<MMScores> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException;

    MMScores resumeGamesPlayed(String playerName) throws ScoreException;

    void addMMScores(MMScores score) throws ScoreException;

    void updateMMScores(MMScores score) throws ScoreException;

    List<MMScores> getScoresByPlayerName(String playerName) throws ScoreException;




}

