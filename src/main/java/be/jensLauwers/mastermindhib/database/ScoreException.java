package be.jensLauwers.mastermindhib.database;


public class ScoreException extends IllegalArgumentException{
    public ScoreException(String message) {
        super(message);
    }

    public ScoreException(Throwable cause) {
        super(cause);
    }
}
