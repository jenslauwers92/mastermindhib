package be.jensLauwers.mastermindhib.database;

import java.sql.SQLException;

public class ScoreSQLException extends SQLException {

    public ScoreSQLException(Throwable cause) {
        super(cause);
    }

}