package be.jensLauwers.mastermindhib;

import be.jensLauwers.mastermindhib.database.LevelSQLException;
import be.jensLauwers.mastermindhib.database.MMLevels;
import be.jensLauwers.mastermindhib.database.MMLevelsDAO;

import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.Test;

import javax.persistence.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class MMLevelsDAOTest {

    static MMLevelsDAO levelsDAO= new MMLevelsDAO();
    private EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction et;

    public EntityManager getEm() {

        try {
            this.emf = Persistence.createEntityManagerFactory("course");
            this.em = emf.createEntityManager();
        } catch (PersistenceException pe) {
            pe.printStackTrace();
        }

        return this.em;

    }

    List<Object> cleanUp = new ArrayList<>();
    @AfterEach
    void cleaner(){
        em = getEm();
        et = em.getTransaction();
        et.begin();
        cleanUp.forEach(em::remove);
        et.commit();
        em.close();
    }





    @Test
    void testGetAllLevels() throws SQLException {
        assertEquals(3, levelsDAO.getAllLevels().size());

    }


    @Test
    void testGetLevelById() throws LevelSQLException {
        MMLevels level = levelsDAO.getLevelById(1);
        assertEquals(1, level.getId());
        assertEquals("basic", level.getLevelName());
        assertEquals(1, level.getLowDigit());
        assertEquals(6, level.getHighDigit());
        assertEquals(4, level.getNumOfDigits());
        assertTrue(level.isAllowDoubles());

    }

}

