package be.jensLauwers.mastermindhib;

import be.jensLauwers.mastermindhib.database.LevelException;
import be.jensLauwers.mastermindhib.database.MMLevels;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;


class MMLevelsTest {
    MMLevels gameLevel;


    @BeforeEach
    void init() {
        gameLevel = new MMLevels("normal", 1, 6, 4, false);
    }


    @Test
    void setId() {
        assertThrows(LevelException.class, () -> gameLevel.setId(-1));
        gameLevel.setId(500);
        assertEquals(500, gameLevel.getId());
    }


    @Test
    void setLevelName() {
        assertThrows(LevelException.class, () -> gameLevel.setLevelName(null));

        gameLevel.setLevelName("The best level");
        assertEquals("The best level", gameLevel.getLevelName());

        gameLevel.setLevelName("The worst level");
        assertEquals("The worst level", gameLevel.getLevelName());

        gameLevel.setLevelName("A good level");
        assertEquals("A good level", gameLevel.getLevelName());
    }


    @Test
    void setLowDigit() {
        assertThrows(IllegalArgumentException.class, () -> gameLevel.setLowDigit(-1));

        gameLevel.setHighDigit(4);
        assertThrows(LevelException.class, () -> gameLevel.setLowDigit(5));

        gameLevel.setLowDigit(2);
        assertEquals(2, gameLevel.getLowDigit());

        gameLevel.setLowDigit(3);
        assertEquals(3, gameLevel.getLowDigit());
    }


    @Test
    void setHighDigit() {
        assertThrows(LevelException.class, () -> gameLevel.setHighDigit(-1));

        gameLevel.setHighDigit(2);
        assertEquals(2, gameLevel.getHighDigit());

        gameLevel.setHighDigit(3);
        assertEquals(3, gameLevel.getHighDigit());

        gameLevel.setHighDigit(4);
        assertEquals(4, gameLevel.getHighDigit());
    }


    @Test
    void setNumOfDigits() {
        assertThrows(LevelException.class, () -> gameLevel.setNumOfDigits(0));

        gameLevel.setNumOfDigits(1);
        assertEquals(1, gameLevel.getNumOfDigits());

        gameLevel.setNumOfDigits(2);
        assertEquals(2, gameLevel.getNumOfDigits());

        gameLevel.setNumOfDigits(3);
        assertEquals(3, gameLevel.getNumOfDigits());
    }

    @Test
    void setAllowDoubles() {
        assertFalse(gameLevel.isAllowDoubles());

        gameLevel.setAllowDoubles(true);
        assertTrue(gameLevel.isAllowDoubles());
    }
}
