package be.jensLauwers.mastermindhib;
import be.jensLauwers.mastermindhib.database.LevelSQLException;
import be.jensLauwers.mastermindhib.database.MMLevelsDAO;
import be.jensLauwers.mastermindhib.database.MMScores;
import be.jensLauwers.mastermindhib.database.MMScoresDAO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MMScoresDAOTest {


    static MMScoresDAO scoresDAO = new MMScoresDAO();
    static MMLevelsDAO levelsDAO = new MMLevelsDAO();


    @Test
    void testGetScoreById() {


        MMScores dbScore = scoresDAO.getScoreById(1);

        assertEquals(dbScore.getPlayer(),scoresDAO.getScoreById(1).getPlayer());
        assertEquals(dbScore.getNumberOfGuesses(),scoresDAO.getScoreById(1).getNumberOfGuesses());
        assertEquals(dbScore.getPlayedGames(),scoresDAO.getScoreById(1).getPlayedGames());
        assertEquals(dbScore.getGameTime(),scoresDAO.getScoreById(1).getGameTime());
        assertEquals(dbScore.getMmLevel().getId(),scoresDAO.getScoreById(1).getMmLevel().getId());
    }

    @Test
    void testAddMMScores() throws LevelSQLException {
        MMScores testScore = new MMScores();
        testScore.setPlayer("Jens");
        testScore.setNumberOfGuesses(1);
        testScore.setPlayedGames(3);
        testScore.setGameTime("00:00:05");
        testScore.setMmLevel(levelsDAO.getLevelById(3));

        scoresDAO.addMMScores(testScore);
        MMScores score = scoresDAO.getScoreById(6);

        assertEquals(6, score.getId());
        assertEquals("Jens", score.getPlayer());
        assertEquals(1, score.getNumberOfGuesses());
        assertEquals(3, score.getPlayedGames());
        assertEquals("00:00:05", score.getGameTime());
        assertEquals(3, score.getMmLevel().getId());
    }

    @Test
    void testUpdateMMScores() throws LevelSQLException {

        MMScores updateScore = new MMScores( "updatedName", 100, 100, "00:01:00");
        updateScore.setId(1);
        updateScore.setMmLevel(levelsDAO.getLevelById(1));
        scoresDAO.updateMMScores(updateScore);
        MMScores testScore = scoresDAO.getScoreById(1);
        assertEquals("updatedName", testScore.getPlayer());
    }

    @Test
    void testGetScoresByPlayerName() {
        assertEquals(2, scoresDAO.getScoresByPlayerName("Jens").size());
        assertNotEquals(2, scoresDAO.getScoresByPlayerName("Dirk").size());
    }

    @Test
    void testGetHighscorePerLevel() {
        MMScores highscoreLevel1 = scoresDAO.getHighscorePerLevel(1);
        MMScores highscoreLevel2 = scoresDAO.getHighscorePerLevel(2);
        MMScores highscoreLevel3 = scoresDAO.getHighscorePerLevel(3);

        assertNotEquals(scoresDAO.getScoreById(3), highscoreLevel1 );
        assertNotEquals(scoresDAO.getScoreById(10), highscoreLevel2 );
        assertNotEquals(scoresDAO.getScoreById(8), highscoreLevel3 );
    }

    @Test
    void testResumeGamesPlayed() {
        MMScores test = scoresDAO.getScoreById(2);
        assertEquals(test.getGameTime(), scoresDAO.resumeGamesPlayed("Jens").getGameTime());

        MMScores test2 = scoresDAO.getScoreById(3);
        assertEquals(test2.getGameTime(), scoresDAO.resumeGamesPlayed("Dirk").getGameTime());
    }
}


