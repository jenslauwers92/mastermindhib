package be.jensLauwers.mastermindhib;

import be.jensLauwers.mastermindhib.database.MMScores;
import be.jensLauwers.mastermindhib.database.ScoreException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MMScoresTest {
    MMScores score;

    @BeforeEach
    void init() {
        score = new MMScores("Some person", 5, 4, "00:01:00");
    }

    @Test
    void setId() {
        assertThrows(ScoreException.class, () -> score.setId(-1));

        score.setId(500);
        assertEquals(500, score.getId());
    }

    @Test
    void setPlayer() {
        assertThrows(ScoreException.class, () -> score.setPlayer(null));
        assertThrows(ScoreException.class, () -> score.setPlayer(""));

        score.setPlayer("A player");
        assertEquals("A player", score.getPlayer());
    }

    @Test
    void setNumberOfGuesses() {
        assertThrows(ScoreException.class, () -> score.setNumberOfGuesses(-1));

        score.setNumberOfGuesses(100);
        assertEquals(100, score.getNumberOfGuesses());
    }


    @Test
    void setGamesPlayed() {
        assertThrows(ScoreException.class, () -> score.setPlayedGames(-1));

        score.setPlayedGames(250);
        assertEquals(250, score.getPlayedGames());
    }

    @Test
    void setTimePlayed() {
        assertThrows(ScoreException.class, () -> score.setGameTime(null));
        assertThrows(ScoreException.class, () -> score.setGameTime(null));

        score.setGameTime("00:01:00");
        assertEquals("00:01:00", score.getGameTime());
    }
}

