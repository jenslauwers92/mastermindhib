package be.jensLauwers.mastermindhib;

import be.jensLauwers.mastermindhib.database.LevelSQLException;
import be.jensLauwers.mastermindhib.database.MMLevels;
import be.jensLauwers.mastermindhib.database.MMLevelsDAO;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MastermindTest {
    MMLevelsDAO mmLevelsDAO = new MMLevelsDAO();
    Mastermind mm = new Mastermind();


    @BeforeEach
    void resetMastermind() throws LevelSQLException {

        mm.setGameLevel(mmLevelsDAO.getLevelById(1));
        mm.reset(mm.getGameLevel());
    }

    /**
     * Test 1
     * Checks the setFinishedGameTest method is works fine or not
     */
    @Test
    void setFinishedGameTest() {
        assertEquals(0, mm.getGuesses());
        assertFalse(mm.isFinishedGame());
        assertEquals("++++", mm.checkGuess(mm.getAnswer(), mm.getGameLevel()));
        assertEquals(1, mm.getGuesses());
        assertTrue(mm.isFinishedGame());
        //# no more guesses after finish
        assertEquals(Mastermind.THE_GAME_FINISHED, mm.checkGuess(mm.getAnswer(), mm.getGameLevel()));
        assertEquals(1, mm.getGuesses());
    }


    @RepeatedTest(value=10)
    void resetTest() {
        assertEquals(0, mm.getGuesses());
        assertFalse(mm.isFinishedGame());
        //# get answer and finish te game
        String answer = mm.getAnswer();
        mm.checkGuess(answer, mm.getGameLevel());
        assertTrue(mm.isFinishedGame());
        assertEquals(1, mm.getGuesses());

        //# reset te game
        mm.reset(mm.getGameLevel());

        //# new answer, guesses back to 0 and not finished
        assertEquals(0, mm.getGuesses());
        assertNotEquals(answer, mm.getAnswer());
        assertFalse(mm.isFinishedGame());
    }


    @Test
    void setAnswerTest() {
        assertThrows(IllegalArgumentException.class, () -> mm.setAnswer(null));
        assertThrows(IllegalArgumentException.class, () -> mm.setAnswer(""));
        assertThrows(IllegalArgumentException.class, () -> mm.setAnswer("abcd"));
        assertThrows(IllegalArgumentException.class, () -> mm.setAnswer("123"));
        assertThrows(IllegalArgumentException.class, () -> mm.setAnswer("12345"));
        assertThrows(IllegalArgumentException.class, () -> mm.setAnswer("12a34"));

        assertDoesNotThrow(()-> mm.setAnswer("1234"));
        assertEquals("1234", mm.getAnswer());
        assertDoesNotThrow(()-> mm.setAnswer("3456"));
        assertEquals("3456", mm.getAnswer());
        assertDoesNotThrow(()-> mm.setAnswer("6543"));
        assertEquals("6543", mm.getAnswer());
        assertDoesNotThrow(()-> mm.setAnswer("4321"));
        assertEquals("4321", mm.getAnswer());

        mm.setGameLevel(new MMLevels("expert", 0, 9, 6, true));
        mm.setAnswer("546495");
        assertEquals("546495", mm.getAnswer());
    }


    @Test
    void setGameLevelTest() {
        assertThrows(IllegalArgumentException.class, () -> mm.setGameLevel(null));
        assertThrows(IllegalArgumentException.class,
                () -> mm.setGameLevel(new MMLevels( "ultimate", -20, 40, 5, false)));

        mm.setGameLevel(new MMLevels("ultimate", 5, 40, 9, true));
        assertEquals("ultimate", mm.getGameLevel().getLevelName());
        assertEquals(5, mm.getGameLevel().getLowDigit());
        assertEquals(40, mm.getGameLevel().getHighDigit());
        assertEquals(9, mm.getGameLevel().getNumOfDigits());
        assertTrue(mm.getGameLevel().isAllowDoubles());
    }


    @Test
    void checkGuessTest() throws LevelSQLException {
        // check with new MMLevels(1, "normal", 1, 6, 4, false)
        mm.setAnswer("1234");

        assertThrows(IllegalArgumentException.class, () -> mm.checkGuess(null, mm.getGameLevel()));
        assertThrows(IllegalArgumentException.class, () -> mm.checkGuess("", mm.getGameLevel()));

        assertEquals("++", mm.checkGuess("1256", mm.getGameLevel()));
        assertEquals("++", mm.checkGuess("  1256  ", mm.getGameLevel()));
        assertEquals("++", mm.checkGuess("  1  2  5  6  ", mm.getGameLevel()));
        assertEquals("++--", mm.checkGuess("1243", mm.getGameLevel()));
        assertEquals("--", mm.checkGuess("4356", mm.getGameLevel()));
        assertEquals("+", mm.checkGuess("1111", mm.getGameLevel()));
        assertEquals("++++", mm.checkGuess("1234", mm.getGameLevel()));
        assertEquals(Mastermind.THE_GAME_FINISHED, mm.checkGuess("5423", mm.getGameLevel()));

        // check with new MMLevels(2, "advanced", 1, 8, 6, false)
        mm.setGuesses(0);
        mm.setFinishedGame(false);

        mm.setGameLevel(mmLevelsDAO.getLevelById(2));
        mm.setAnswer("123456");

        assertThrows(IllegalArgumentException.class, () -> mm.checkGuess("4", mm.getGameLevel()));
        assertThrows(IllegalArgumentException.class, () -> mm.checkGuess("1236584", mm.getGameLevel()));
        assertThrows(IllegalArgumentException.class, () -> mm.checkGuess("87654g", mm.getGameLevel()));

        assertEquals("++++--", mm.checkGuess("213456", mm.getGameLevel()));
        assertEquals("++---", mm.checkGuess("158463", mm.getGameLevel()));
        assertEquals("", mm.checkGuess("888888", mm.getGameLevel()));
        assertEquals("------", mm.checkGuess("456321", mm.getGameLevel()));
        assertEquals("-----", mm.checkGuess("458321", mm.getGameLevel()));
        assertEquals("++++++", mm.checkGuess("123456", mm.getGameLevel()));
        assertEquals(Mastermind.THE_GAME_FINISHED, mm.checkGuess("158463", mm.getGameLevel()));

        // check with new MMLevels(3, "expert", 0, 9, 6, true)
        mm.setGuesses(0);
        mm.setFinishedGame(false);

        mm.setGameLevel(mmLevelsDAO.getLevelById(3));
        mm.setAnswer("847533");
        assertEquals("----", mm.checkGuess("123456", mm.getGameLevel()));
        assertEquals("------", mm.checkGuess("335748", mm.getGameLevel()));
        assertEquals("+---", mm.checkGuess("738203", mm.getGameLevel()));
        assertEquals("--", mm.checkGuess("685096", mm.getGameLevel()));
        assertEquals("+-", mm.checkGuess("003030", mm.getGameLevel()));
        assertEquals("++++++", mm.checkGuess("847533", mm.getGameLevel()));
        assertEquals(Mastermind.THE_GAME_FINISHED, mm.checkGuess("906325", mm.getGameLevel()));

        // check with new MMLevels(3, "expert", 0, 9, 6, true)
        mm.setGuesses(0);
        mm.setFinishedGame(false);

        mm.setGameLevel(mmLevelsDAO.getLevelById(3));
        mm.setAnswer("658535");
        assertEquals("++--", mm.checkGuess("957655", mm.getGameLevel()));
        assertEquals("+++", mm.checkGuess("555555", mm.getGameLevel()));
        assertEquals("++-", mm.checkGuess("768095", mm.getGameLevel()));
        assertEquals("------", mm.checkGuess("535856", mm.getGameLevel()));
        assertEquals("++++++", mm.checkGuess("658535", mm.getGameLevel()));
    }


    @Test
    void setGuessesTest() {
        assertThrows(IllegalArgumentException.class, () -> mm.setGuesses(-5));

        mm.setGuesses(0);
        assertEquals(0, mm.getGuesses());

        mm.setGuesses(100);
        assertEquals(100, mm.getGuesses());
    }


    @Test
    void setInvalidGuessStructureTest() {
        assertThrows(IllegalArgumentException.class, () -> mm.setInvalidGuessStructure(null));
        mm.setInvalidGuessStructure("Hello");
        assertEquals("Hello", mm.getInvalidGuessStructure());
    }


    @Test
    void createRandomAnswerTest() throws LevelSQLException {
        assertThrows(IllegalArgumentException.class, () -> mm.createRandomAnswer(null));

        mm.setGameLevel(mmLevelsDAO.getLevelById(1));
        mm.createRandomAnswer(mm.getGameLevel());
        assertEquals(4, mm.getAnswer().length());
    }


    @Test
    void checkNumberOfGuessesTest() {
        //# check number of guesses
        mm.setAnswer("1234");
        assertEquals(0, mm.getGuesses());
        mm.checkGuess("1256", mm.getGameLevel());
        assertEquals(1, mm.getGuesses());
        mm.checkGuess("1243", mm.getGameLevel());
        assertEquals(2, mm.getGuesses());
        mm.checkGuess("4356", mm.getGameLevel());
        assertEquals(3, mm.getGuesses());

    }
}

